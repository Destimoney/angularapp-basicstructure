
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsComponent } from './charts.component';
import { ChartsModule } from 'ng2-charts';


const routes: Routes = [

    { path: '', component: ChartsComponent }
]



@NgModule({
    imports: [
        CommonModule,
        ChartsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ChartsComponent]
})

export class ChartComponentModule { }