// import { MasterDumpComponent } from './masterDump';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';


const routes: Routes = [

    { path: '', component: HomeComponent }
]



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [HomeComponent]
})

export class HomeModule { }