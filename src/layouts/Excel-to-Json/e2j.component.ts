import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-e2j',
  templateUrl: './e2j.component.html',
  styleUrls: ['./e2j.component.scss']
})
export class Excel2JsonComponent {

  data: any;

  constructor( ) { }




  onFileChange(evt) {


    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();

    reader.onload = (e: any) => {

      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
      console.log("Data shown " + JSON.stringify(this.data))
    };
    console.log(reader.readAsBinaryString(target.files[0]))






  
  }





}