// import { MasterDumpComponent } from './masterDump';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Excel2JsonComponent } from './e2j.component';


const routes: Routes = [

    { path: '', component: Excel2JsonComponent }
]



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [Excel2JsonComponent]
})

export class Excel2JsonModule { }